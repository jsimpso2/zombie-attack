package zombieattack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZombieAttack
{
	public static void main ( String[] args )
	{
		SpringApplication.run( ZombieAttack.class, args );
	}
}
