package zombieattack;

import org.springframework.stereotype.Service;
import zombieattack.annotations.AchievementTracker;
import zombieattack.annotations.KillCounter;
import zombieattack.annotations.SpeedTracker;
import zombieattack.annotations.WordLengthTracker;

import java.time.LocalDateTime;

@Service
public class ZombieService
{
    @KillCounter
    @WordLengthTracker
    @SpeedTracker
    @AchievementTracker
    public void killZombie( String word )
    {
        System.out.println( "Killed '" + word + "'" );
        GameInfo.addKill( word, LocalDateTime.now() );
    }
}
