package zombieattack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import zombieattack.domain.AttackResponse;
import zombieattack.util.Filter;

@Controller
public class ZombieController
{
    @Autowired
    private ZombieService zombieService;

    @RequestMapping( value = "attack", method = RequestMethod.POST )
    @ResponseBody
    public AttackResponse attack( @RequestParam( "word" ) String word, @RequestParam( "typed" ) String typed )
    {
        AttackResponse attackResponse = new AttackResponse();
        if ( word.equals( typed ) )
        {
            zombieService.killZombie( word );

            attackResponse.setNewWord( GameInfo.getNewWord() );
            attackResponse.setKillCount( GameInfo.getKillCount() );
            attackResponse.setAchievements( GameInfo.resetNewAchievements() );
        }

        return attackResponse;
    }

    @RequestMapping( value = "start", method = RequestMethod.POST )
    @ResponseBody
    public AttackResponse start()
    {
        Filter.loadConfigs();
        GameInfo.reset();

        AttackResponse attackResponse = new AttackResponse();
        attackResponse.setNewWord( GameInfo.getNewWord() );

        return attackResponse;
    }
}
