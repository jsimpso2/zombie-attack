package zombieattack.domain;

import java.util.ArrayList;
import java.util.List;

public class AttackResponse
{
	private String newWord = "";
	private List<Achievement> achievements = new ArrayList<>();
	private int killCount = 0;

	public String getNewWord()
	{
		return newWord;
	}

	public void setNewWord( String newWord )
	{
		this.newWord = newWord;
	}

	public List<Achievement> getAchievements()
	{
		return achievements;
	}

	public void setAchievements( List<Achievement> achievements )
	{
		this.achievements = achievements;
	}

	public int getKillCount()
	{
		return killCount;
	}

	public void setKillCount( int killCount )
	{
		this.killCount = killCount;
	}
}
