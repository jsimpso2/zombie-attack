package zombieattack.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Achievement
{
    Noob( "Noob", "Get your first kill!", Level.None ),
    Decathlon( "Decathlon", "Get 10 kills.", Level.Bronze ),
    Quarter( "Half of Half of 100", "Get 25 kills.", Level.Silver ),
    FiftyShades( "Fifty Shades of Red", "Get 50 kills.", Level.Gold ),

    Snack( "Snack", "Get 2 kills in 4 seconds.", Level.None ),
    Wendys( "Wendy's 4 for 4", "Get 4 kills in 4 seconds.", Level.Bronze ),
    Supersized( "Supersized", "Get 6 kills in 4 seconds.", Level.Silver ),
    TableForTwo( "Table for 2", "Get 8 kills in 4 seconds.", Level.Gold ),

    Jack( "Jack", "Type a 5-letter word.", Level.None ),
    Queen( "Queen", "Type a 6-letter word.", Level.Bronze ),
    King( "King", "Type a 7-letter word.", Level.Silver ),
    Ace( "Ace", "Type an 8-letter word.", Level.Gold ),

    Collector( "Collector", "Get 3 achievements.", Level.None ),
    Enthusiast( "Enthusiast", "Get 6 achievements.", Level.Bronze ),
    Fanatic( "Fanatic", "Get 10 achievements.", Level.Silver ),
    SoleProprietor( "Sole Proprietor", "Get 15 achievements.", Level.Gold );

    private final String name;
    private final String description;
    private final Level level;

    Achievement( String name, String description, Level level )
    {
        this.name = name;
        this.description = description;
        this.level = level;
    }

    @JsonProperty( "name" )
    public String getName()
    {
        return this.name;
    }

    @JsonProperty( "description" )
    public String getDescription()
    {
        return this.description;
    }

    @JsonProperty( "level" )
    public String getLevel()
    {
        return this.level.name().toLowerCase();
    }

    public enum Level
    {
        None, Bronze, Silver, Gold
    }
}