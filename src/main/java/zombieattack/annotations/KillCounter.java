package zombieattack.annotations;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import zombieattack.GameInfo;
import zombieattack.domain.Achievement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target( ElementType.METHOD )
public @interface KillCounter
{
    @Aspect
    @Component
    @Order( 2 )
    class AchievementHandler
    {
        @After( "@annotation( KillCounter )" )
        public void countKills()
        {
            System.out.println( "\t@After - counting kills" );
            switch ( GameInfo.getKillCount() )
            {
                case 1:
                    GameInfo.addAchievement( Achievement.Noob );
                    break;
                case 10:
                    GameInfo.addAchievement( Achievement.Decathlon );
                    break;
                case 25:
                    GameInfo.addAchievement( Achievement.Quarter );
                    break;
                case 50:
                    GameInfo.addAchievement( Achievement.FiftyShades );
                    break;
            }
        }
    }
}
