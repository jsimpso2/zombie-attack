package zombieattack.annotations;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import zombieattack.GameInfo;
import zombieattack.domain.Achievement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target( ElementType.METHOD )
public @interface WordLengthTracker
{
    @Aspect
    @Component
    class AchievementHandler
    {
        @Before( "@annotation( WordLengthTracker ) && args( word )" )
        public void trackWordLength( String word )
        {
            System.out.println( "\t@Before - tracking word length" );
            switch ( word.length() )
            {
                case 5:
                    GameInfo.addAchievement( Achievement.Jack );
                    break;
                case 6:
                    GameInfo.addAchievement( Achievement.Queen );
                    break;
                case 7:
                    GameInfo.addAchievement( Achievement.King );
                    break;
                case 8:
                    GameInfo.addAchievement( Achievement.Ace );
                    break;
            }
        }
    }
}
