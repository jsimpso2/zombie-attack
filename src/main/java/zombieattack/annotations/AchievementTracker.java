package zombieattack.annotations;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import zombieattack.GameInfo;
import zombieattack.domain.Achievement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target( ElementType.METHOD )
public @interface AchievementTracker
{
    @Aspect
    @Component
    @Order( 1 )
    class AchievementHandler
    {
        @After( "@annotation( AchievementTracker )" )
        public void trackAchievements()
        {
            System.out.println( "\t@After - tracking achievements" );
            switch ( GameInfo.getPreviousAchievements().size() + GameInfo.getNewAchievements().size() )
            {
                case 3:
                    GameInfo.addAchievement( Achievement.Collector );
                    break;
                case 6:
                    GameInfo.addAchievement( Achievement.Enthusiast );
                    break;
                case 10:
                    GameInfo.addAchievement( Achievement.Fanatic );
                    break;
                case 15:
                    GameInfo.addAchievement( Achievement.SoleProprietor );
                    break;
            }
        }
    }
}
