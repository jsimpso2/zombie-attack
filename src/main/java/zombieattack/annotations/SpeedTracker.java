package zombieattack.annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import zombieattack.GameInfo;
import zombieattack.domain.Achievement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Target( ElementType.METHOD )
public @interface SpeedTracker
{
    @Aspect
    @Component
    class AchievementHandler
    {
        @Around( "@annotation( SpeedTracker )" )
        public Object trackSpeed( ProceedingJoinPoint call ) throws Throwable
        {
            System.out.println( "\t@Around - defining time threshold" );
            LocalDateTime threshold = LocalDateTime.now().minusSeconds( 4 );

            Object result = call.proceed();

            System.out.println( "\t@Around - tracking speed");
            List<LocalDateTime> lastKills = new ArrayList<>();
            for ( LocalDateTime killTime : GameInfo.getKills().values() )
            {
                if ( killTime.isAfter( threshold ) )
                {
                    lastKills.add( killTime );
                }
            }

            switch ( lastKills.size() )
            {
                case 2:
                    GameInfo.addAchievement( Achievement.Snack );
                    break;
                case 4:
                    GameInfo.addAchievement( Achievement.Wendys );
                    break;
                case 6:
                    GameInfo.addAchievement( Achievement.Supersized );
                    break;
                case 8:
                    GameInfo.addAchievement( Achievement.TableForTwo );
                    break;
            }

            return result;
        }
    }
}
