package zombieattack;

import com.abcfinancial.common.Random;
import zombieattack.domain.Achievement;
import zombieattack.util.Filter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameInfo
{
    private static Map<String, LocalDateTime> kills = new HashMap<>();
    private static List<Achievement> previousAchievements = new ArrayList<>();
    private static List<Achievement> newAchievements = new ArrayList<>();

    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 9;

    public static void reset()
    {
        kills = new HashMap<>();
        previousAchievements = new ArrayList<>();
        newAchievements = new ArrayList<>();
    }

    public static String getNewWord()
    {
        int median = (getKillCount() / 6);

        int minLength = Random.getInt( MIN_LENGTH, Math.max( MIN_LENGTH + 1, median + getRandomDeviant() ) );
        int maxLength = Random.getInt( Math.min( median + getRandomDeviant(), MAX_LENGTH - 1 ), MAX_LENGTH );

        minLength = minLength == 0 ? 1 : minLength;
        maxLength = maxLength == 0 ? 1 : maxLength;

        minLength = Math.max( MIN_LENGTH, minLength );
        maxLength = Math.min( maxLength, MAX_LENGTH );

        minLength = Math.min( minLength, maxLength );

        if ( minLength == maxLength )
        {
            if ( minLength == MIN_LENGTH )
            {
                ++maxLength;
            }
            else
            {
                --minLength;
            }
        }

        return getNewWord( Random.getInt( minLength, maxLength ) );
    }

    private static String getNewWord( int length )
    {
        String newWord;

        do
        {
            newWord = Random.getString( length );
        }
        while ( Filter.badWordsFound( newWord ) );

        System.out.println( "Zombie Attack!! '" + newWord + "'" );
        return newWord;
    }

    private static int getRandomDeviant()
    {
        return Random.getInt( 0, 1 );
    }

    public static void addAchievement( Achievement achievement )
    {
        if ( !previousAchievements.contains( achievement ) )
        {
            newAchievements.add( achievement );
            System.out.println( ">> " + achievement.getName() );
        }
    }

    public static void addKill( String word, LocalDateTime time )
    {
        kills.put( word, time );
    }

    public static List<Achievement> resetNewAchievements()
    {
        List<Achievement> achievements = new ArrayList<>();
        achievements.addAll( newAchievements );
        previousAchievements.addAll( newAchievements );
        newAchievements = new ArrayList<>();
        return achievements;
    }

    public static Map<String, LocalDateTime> getKills()
    {
        return kills;
    }

    public static int getKillCount()
    {
        return kills.size();
    }

    public static List<Achievement> getPreviousAchievements()
    {
        return previousAchievements;
    }

    public static List<Achievement> getNewAchievements()
    {
        return newAchievements;
    }
}