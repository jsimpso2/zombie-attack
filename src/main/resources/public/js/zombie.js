function start() {
    $.ajax({
        type: 'POST',
        url: '/zombie/start',
        data: {},
        dataType : 'json',
        cache: false,
        success: getWordCallback
    });
}

function bang() {
    var word = $('#word').text();
    var typed = $('#typing').val();
    getWord(word, typed);
}

function getWord(word, typed) {
    $.ajax({
        type: 'POST',
        url: '/zombie/attack',
        data: {'word' : word,'typed': typed},
        dataType : 'json',
        cache: false,
        success: getWordCallback
    });
}

function getWordCallback(result) {
    if ( result.newWord ) {
        $( '#word' ).text( result.newWord );
        $( '#typing' ).val( '' );
        $( '#killCount' ).text( result.killCount );
        addAchievements( result.achievements );

        $( '#zombie' ).stop( true ).removeAttr( 'style' ).hide().css( {
            position: "relative",
            top: "20px",
            float: "right",
            padding: "5px",
            display: "inline-block",
            height: "200px",
            width: "133px"
        } ).animate( { left: "-=450", height: "+=100", width: "+=66", top: "-=15" }, 5000, gameOver ).fadeIn();
    }
}

function addAchievements( achievements ) {
    if ( achievements && achievements.length ) {
        $.each( achievements, function( index, achievement ) {
            $( '#achievementsHeader' ).after(
               "<tr hidden achievementName='" + achievement.name + "'>" +
               "  <td><div class='achievementLevel " + achievement.level + "'>&nbsp;</div></td>" +
               "  <td><div class='achievementTitle labelFont'>" + achievement.name + "</div></td>" +
               "  <td><div class='achievementDescription dataFont'>" + achievement.description + "</div></td>" +
               "</tr>" );
            $( '#achievements tr[achievementName="' + achievement.name + '"]' ).fadeIn();
        });
    }
}

function gameOver() {
    $( '#gameOver' ).fadeIn( "slow" );
    $( '#typing' ).prop("disabled", true);
    $( '#zombie' )
    .transition( {rotate: '-10deg' } ).transition( {rotate: '10deg' } )
    .transition( {rotate: '-10deg' } ).transition( {rotate: '10deg' } )
    .transition( {rotate: '-10deg' } ).transition( {rotate: '10deg' } )
    .transition( {rotate: '0deg' }, 1000, 'ease' );
}